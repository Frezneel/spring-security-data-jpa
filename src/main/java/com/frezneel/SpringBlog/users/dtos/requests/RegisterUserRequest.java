package com.frezneel.SpringBlog.users.dtos.requests;

import com.frezneel.SpringBlog.roles.entites.Role;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.List;

@Data
public class RegisterUserRequest {

    private String email;

    private String password;

    private String name;

    //single Role
//    private Role role;
    //Multiple Role
    private List<Role> role;
}
