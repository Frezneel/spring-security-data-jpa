package com.frezneel.SpringBlog.users.dtos.responses;

import lombok.Data;

@Data
public class UserResponse {

    private String name;
    private String email;

}
