package com.frezneel.SpringBlog.users.controllers;

import com.frezneel.SpringBlog.users.dtos.requests.RegisterUserRequest;
import com.frezneel.SpringBlog.users.dtos.responses.UserResponse;
import com.frezneel.SpringBlog.users.entites.User;
import com.frezneel.SpringBlog.users.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class RestUser {

    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public ResponseEntity<?> getAllUsers(){
        Map<String, Object> result = new HashMap<>();
        try {
            List<User> users = userService.getAllUsers();
            List<UserResponse> userResponse = users.stream()
                    .map(user -> {
                        UserResponse response = new UserResponse();
                        response.setName(user.getName());
                        response.setEmail(user.getEmail());
                        return response;
                    }).collect(Collectors.toList());
            result.put("status", "200");
            result.put("message", "Data Successfully Acquiring");
            result.put("data", userResponse);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }catch (Exception e){
            result.put("status", "500");
            result.put("message", "Failed Acquiring Data");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/users")
    public ResponseEntity<?> saveUsers(@RequestBody RegisterUserRequest registerUserRequest){
        Map<String, Object> result = new HashMap<>();
        try {
            userService.save(registerUserRequest);
            UserResponse userResponse = new UserResponse();
            userResponse.setEmail(registerUserRequest.getEmail());
            userResponse.setName(registerUserRequest.getName());

            result.put("status", "200");
            result.put("message", "Data Successfully Saved");
            result.put("data", userResponse);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }catch (Exception e){
            result.put("status", "500");
            result.put("message", "Failed Saving Data");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
