package com.frezneel.SpringBlog.users.services.impl;

import com.frezneel.SpringBlog.roles.entites.Role;
import com.frezneel.SpringBlog.users.dtos.requests.RegisterUserRequest;
import com.frezneel.SpringBlog.users.entites.User;
import com.frezneel.SpringBlog.users.repository.UserRepo;
import com.frezneel.SpringBlog.users.services.UserService;
import jakarta.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private UserRepo userRepo;
    private BCryptPasswordEncoder passwordEncoder;

    private EntityManager entityManager;

    @Autowired
    public UserServiceImpl(UserRepo userRepo, BCryptPasswordEncoder passwordEncoder, EntityManager entitiyManager) {
        this.userRepo = userRepo;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepo.findAll();
    }

    @Override
    public Optional<User> getUserById(Long id) {
        return userRepo.findById(id);
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        return userRepo.findByEmail(email);
    }

    @Override
    public void save(RegisterUserRequest registerUserRequest) {
        User user = new User();
        // Single Role Add
//        Role role = registerUserRequest.getRole();
//        user.setRoles(Arrays.asList(role));
        // Multiple Role Add
        Collection<Role> list = registerUserRequest.getRole().stream().collect(Collectors.toList());
        user.setName(registerUserRequest.getName());
        user.setEmail(registerUserRequest.getEmail());
        user.setPassword(passwordEncoder.encode(registerUserRequest.getPassword()));
        userRepo.save(user);
    }

    @Override
    //Method ini milik spring boot security
    //Jadi disini memodifikasi untuk validasi user login
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = getUserByEmail(username);
        if (user.isEmpty()){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        // disini bentuk user pada spring security
        // terdapat username => dirubah ke email untuk username mengambil dari data JPA
        // terdapat password => mengambil dari data JPA
        // terdapat Authorities/Role => ini berbentuk Collection extends GrantedAuthority sehingga pada user memiliki lebih dari 1 role
        return new org.springframework.security.core.userdetails
                .User(user.get().getEmail(), user.get().getPassword()
                , mapRolesToAuthorities(user.get().getRoles()));
    }

    //Untuk mengambil Role pada user, kemudian dibentuk ke collection
    // converter untuk mengambil tabel nama pada role saja kemudian dibuat ke bentuk Collection extetends GrantedAuthority
    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
        return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());
    }

}
