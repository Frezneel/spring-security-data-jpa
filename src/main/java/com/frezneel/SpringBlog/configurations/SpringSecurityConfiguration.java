package com.frezneel.SpringBlog.configurations;

import com.frezneel.SpringBlog.users.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfiguration {

    @Autowired
    private UserService userService;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception{
        http.authorizeHttpRequests(request -> request
                .requestMatchers("/","/home").permitAll()
                .requestMatchers(HttpMethod.GET,"/api/users").hasRole("USER")
                .requestMatchers(HttpMethod.POST,"/api/users").hasRole("ADMIN")
                .anyRequest().authenticated()
        ).csrf(csrf->csrf.disable())
        .logout(logout -> logout.permitAll());
        http.httpBasic();

        return http.build();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider(UserService userService){
        DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
        //Untuk mengatur pemanggilan user pada spring security UserDetailsService
        //Untuk mengisi/membuat akun/user ke format yang dimiliki spring security
        auth.setUserDetailsService(userService);
        //Untuk mengatur encoder/decoder password auth spring security
        auth.setPasswordEncoder(passwordEncoder());
        return auth;
    }
}
