package com.frezneel.SpringBlog.roles.repository;

import com.frezneel.SpringBlog.roles.entites.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<Role, Long> {
}
