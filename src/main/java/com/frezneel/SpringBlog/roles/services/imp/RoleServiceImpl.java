package com.frezneel.SpringBlog.roles.services.imp;

import com.frezneel.SpringBlog.roles.entites.Role;
import com.frezneel.SpringBlog.roles.repository.RoleRepo;
import com.frezneel.SpringBlog.roles.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepo roleRepo;

    @Override
    public List<Role> getAllRole() {
        return roleRepo.findAll();
    }
}
