package com.frezneel.SpringBlog.roles.services;

import com.frezneel.SpringBlog.roles.entites.Role;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RoleService {

    List<Role> getAllRole();
}
