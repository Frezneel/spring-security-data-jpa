-- public.users definition
-- Drop table
DROP TABLE IF exists public.users CASCADE;
CREATE TABLE public.users (
	id bigserial NOT NULL,
	created_at timestamp(6) NULL,
	email varchar(255) NULL,
	"name" varchar(255) NULL,
	"password" varchar(255) NULL,
	updated_at timestamp(6) NULL,
	CONSTRAINT uk_6dotkott2kjsp8vw4d0m25fb7 UNIQUE (email),
	CONSTRAINT users_pkey PRIMARY KEY (id)
);

-- public."role" definition
-- Drop table
DROP table IF EXISTS public."role" CASCADE;
-- create table
CREATE TABLE public."role" (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	CONSTRAINT role_pkey PRIMARY KEY (id)
);

-- public.users_roles definition
-- Drop table
DROP table IF EXISTS public.users_roles CASCADE;
CREATE TABLE public.users_roles (
	user_id int8 NOT NULL,
	role_id int8 NOT NULL,
	CONSTRAINT fk2o0jvgh89lemvvo17cbqvdxaa FOREIGN KEY (user_id) REFERENCES public.users(id),
	CONSTRAINT fkt4v0rrweyk393bdgt107vdx0x FOREIGN KEY (role_id) REFERENCES public."role"(id)
);

INSERT INTO "role" ("name")
VALUES
('ROLE_ADMIN'),
('ROLE_MANAGER'),
('ROLE_USER');

INSERT INTO "users" ("created_at","email","name","password","updated_at")
VALUES
(now(), 'galih@gmail.com','Galih Muhammad Ichsan','$2a$10$U7oAtvQ19NgMLnFEo6wJFuKNYE8rXNArzansqoQIdvWET2PFUArMS',now()),
(now(), 'john@gmail.com','John','$2a$04$eFytJDGtjbThXa80FyOOBuFdK2IwjyWefYkMpiBEFlpBwDH.5PM0K',now()),
(now(), 'mary@gmail.com','mary','$2a$04$eFytJDGtjbThXa80FyOOBuFdK2IwjyWefYkMpiBEFlpBwDH.5PM0K',now()),
(now(), 'susan@gmail.com','susan','$2a$04$eFytJDGtjbThXa80FyOOBuFdK2IwjyWefYkMpiBEFlpBwDH.5PM0K',now());

INSERT INTO "users_roles" ("user_id","role_id")
VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 2),
(2, 3),
(3, 3),
(4, 3);
