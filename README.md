# Spring Security Data JPA

## Getting started
## *Required*
1. Java JDK 17
2. Lombok Plugin
3. PostgreSQL
 - note : If using another database, you can change dependency and application.properties setting

## Add your database

1. Open your management database,
2. Then open SQL Script "01-user-database-test",
3. Copy and Run the script, 
4. Clone this project and open in any your IDE, 
5. Then run it.

## How to test?
1. Open Postman software
2. Add endpoint => example http://localhost:8080/api/users 
3. 1. Select GET Method for test USER role.
   2. Select POST Method for test ADMIN role.
4. Select tab Auth then choose type auth to "Basic Auth" and fill username password,
5. 2. Select tab Body, then choose to raw -> JSON
6. 2. Change Input like Format request register, [like at note]
7. Click Send, look the result in bottom at postman UI
- note :
  - For Post method Format Raw/JSON
    ```
    {
      "email": "test@gmail",
      "password": "apaaja12",
      "name": "Nama Panjang Banget",
      "role": [
          { "id": 3,
            "name": "ROLE_USER"},
          { "id": 2,
            "name": "ROLE_MANAGER"},
          { "id": 1,
            "name": "ROLE_ADMIN"}
          ]
    }

## Video Test
- Coming Soon

## Document Reference

- [Project Spring Security Reference](https://drive.google.com/drive/folders/1Wh9N-zTn6xZW2sPsxMecMjgQT-uaNDI3?usp=sharing)